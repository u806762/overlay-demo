import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessThreeComponent } from './process-three.component';

describe('ProcessThreeComponent', () => {
  let component: ProcessThreeComponent;
  let fixture: ComponentFixture<ProcessThreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ProcessThreeComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProcessThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
