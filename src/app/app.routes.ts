import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: 'process-one',
    loadComponent: () => import('./process-one/process-one.component').then(c => c.ProcessOneComponent)
  },
  {
    path: 'process-two',
    loadComponent: () => import('./process-two/process-two.component').then(c => c.ProcessTwoComponent)
  },
  {
    path: 'process-three',
    loadComponent: () => import('./process-three/process-three.component').then(c => c.ProcessThreeComponent)
  },
  {
    path: '',
    redirectTo: 'process-one',
    pathMatch: 'full'
  }
];
