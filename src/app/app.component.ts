import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {MobiUiHeaderComponent, MobiUiOverlayComponent, MobiUiOverlayDirective} from "@mobi/rwc-ui-components-ng-jslib";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, MobiUiHeaderComponent, MobiUiOverlayDirective, MobiUiOverlayComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'overlay-test';
}
