import { Component } from '@angular/core';
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-process-one',
  standalone: true,
  imports: [
    RouterLink
  ],
  templateUrl: './process-one.component.html',
  styleUrl: './process-one.component.scss'
})
export class ProcessOneComponent {

}
