import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessTwoComponent } from './process-two.component';

describe('ProcessTwoComponent', () => {
  let component: ProcessTwoComponent;
  let fixture: ComponentFixture<ProcessTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ProcessTwoComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProcessTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
