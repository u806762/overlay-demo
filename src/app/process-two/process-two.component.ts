import { Component } from '@angular/core';
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-process-two',
  standalone: true,
  imports: [
    RouterLink
  ],
  templateUrl: './process-two.component.html',
  styleUrl: './process-two.component.scss'
})
export class ProcessTwoComponent {

}
